#' List reports with headers
#'
#' @param reports_dir [character] path for the reports
#' @param reports [character] subfolders containing each report.
#' @param index [character] name of the report index file
#'
#' @return A [data.frame] containing one row by report found. Each row contain the following columns:
#'
#'  - `report`: the folder name of the report
#'  - `path`: the full path of the report folder
#'  - and all the metadata located in the header of the report index file: `title`, `author`, ...
#'
#' @importFrom stats setNames
#' @export
#'
list_reports <- function(reports_dir = file.path(pkgload::pkg_path(), "reports"),
                         reports = list.dirs(reports_dir, full.names = FALSE, recursive = FALSE),
                         index = "index.Rmd") {
  if (length(reports) == 0) {
    warning("No report found in ", reports_dir)
    return(NULL)
  }
  paths <- setNames(file.path(reports_dir, reports), reports)
  l <- lapply(paths, get_report_header)
  if (all(sapply(l, is.null))) {
    warning("No report found in ", reports_dir)
    return(NULL)
  }
  df <- do.call(dplyr::bind_rows, l)
  df <- cbind(report = reports[!sapply(l, is.null)],
              path = paths[!sapply(l, is.null)],
              df)
  return(df)
}


#' Get report header
#'
#' @param path path of the report
#' @inheritParams list_reports
#'
#' @return A [list] with header of the report index file
#' @noRd
#'
get_report_header <- function(path, index = "index.Rmd") {
  f <- file.path(path, index)
  if (file.exists(f)) {
    l <- read_rmd(file.path(path, index))
    header <- lapply(l$header, function(x) {
      if (!is.list(x)) {
        return(paste(x, collapse = ", "))
      } else {
        return(NULL)
      }
    })
    return(header)
  } else {
    warning("File ", f, " not found, the folder ", basename(path), " is ignored")
    return(NULL)
  }
}


#' @noRd
#'
#' @title Read R-markdown Documents
#'
#' @description
#' Import Rmd files into objects of class [rmd_doc-class].
#'
#' The function `txt_body()` add a line break at the end of each element of a
#' character vector considering them as single lines.
#'
#' Note that comments will be deleted in the input file.
#'
#' @param file Character value indicating the path and the name to the Rmd file.
#' @param ... Arguments passed by `read_rmd()` to [readLines()].
#'     In `txt_body()` they are character values passed to `c()`.
#' @param skip_head Logical value indicating whether the yaml head should be
#'     skip or not (this argument is not used at the moment).
#'
#' @return
#' The function `read_rmd()` returns a [rmd_doc-class] object.
#' The function `txt_body()`, a character vector suitable for the parameter
#' `body` in the function [write_rmd()].
#'
#' @examples
#' \dontrun{
#' ## Read pre-installed example
#' ex_document <- read_rmd(file.path(
#'   path.package("yamlme"),
#'   "taxlistjourney.Rmd"
#' ))
#' }
#' @source https://github.com/kamapu/yamlme/blob/master/R/read_rmd.R
read_rmd <- function(file, ..., skip_head = FALSE) {
  file <- txt_body(readLines(file, ...))
  if (substr(file[1], 1, 3) != "---") {
    message("Rmd file seems to be headless")
    yaml_head <- list(body = file)
  } else {
    idx <- cumsum(grepl("---", file, fixed = TRUE))
    yaml_head <- list(header = yaml::yaml.load(paste0(file[idx == 1], collapse = "")))
    yaml_head$body <- file[idx > 1][-1]
  }
  if (skip_head) {
    yaml_head$header <- NULL
  }
  class(yaml_head) <- c("rmd_doc", "list")
  return(yaml_head)
}

#' @noRd
txt_body <- function(...) {
  return(paste0(c(...), "\n"))
}
