#' Update or install the fairify functions and default configuration
#' in a project created with fairify
#'
#' @param path Path of the fairify project or inside the fairify project
#'
#' @return Use for side effect.
#' @export
#'
update_fairify <- function(path = ".") {
  path <- pkgload::pkg_path(path)
  b <- file.copy(
    from = list.files(pkg_sys("functions"), full.names = TRUE),
    to = file.path(path, "R"),
    overwrite = TRUE
  )
  if (any(!b)) {
    stop("Error during the copy of fairify functions")
  }
  if (file.exists(file.path(path, "inst", "config.yml"))) {
    warning("config.yml already exists in the target project")
  } else {
    dir.create(file.path(path, "inst"), showWarnings = FALSE)
    b <- file.copy(
      from = pkg_sys("config.yml"),
      to = file.path(path, "inst", "config.yml")
    )
    if (any(!b)) {
      stop("Error during the copy of default configuration")
    }
  }
}
