#' Create a "fairify" project
#'
#' Create a package ready for fairify your project.
#'
#' @inheritParams usethis::create_package
#' @param tidy if `TRUE`, run [usethis::use_testthat()], and [usethis::use_tidy_description()]
#' @param reports If `TRUE`, run `create_reports`, preparing package structure for report publications
#' @param cloud If `TRUE`, copy fairify functions for cloud capabilities
#' @param git if `TRUE`, initialise a Git repository
#' @param pkgdown if `TRUE`, initialise a pkgdown web site (See [usethis::use_pkgdown])
#' @param destdir Target directory for generated pkgdown website and reports
#' @param ... Further parameters passed to [usethis::create_package]
#'
#' @return Use for side effect.
#' @export
#'
#' @examples
#' path <- tempfile(pattern = "dir")
#' create_fairify(path, open = FALSE, git = FALSE)
#' list.files(path)
create_fairify <-
  function(path,
           tidy = TRUE,
           check_name = FALSE,
           open = rlang::is_interactive(),
           reports = TRUE,
           cloud = TRUE,
           git = TRUE,
           pkgdown = TRUE,
           destdir = "public",
           fields = list(Remotes = "git::https://forgemia.inra.fr/umr-g-eau/fairify.git"),
           ...) {

    usethis::create_package(path,
                            fields = fields,
                            open = FALSE,
                            check_name = check_name,
                            ...)
    usethis::proj_set(path)
    usethis::use_package("fairify", "Depends")

    if (cloud) update_fairify(path)

    if (tidy) {
      usethis::local_project(path)
      usethis::use_tidy_description()
    }

    if (git) {
      gert::git_init(path)
      add_gitignore(path)
      file.copy(pkg_sys("gitlab-ci.yml"), file.path(path, ".gitlab-ci.yml"))
    }

    if (reports) create_reports(path = path, git = git)

    if(pkgdown) {
      config_file <- "_pkgdown.yml"
      usethis::use_build_ignore(c(config_file, destdir, "pkgdown"))
      usethis::use_git_ignore(destdir)
      config <- list(url = NULL)
      config$template <- list(bootstrap = 5L)
      config$destination <- destdir
      writeLines(yaml::as.yaml(config), file.path(path, config_file))
    }

    message("Fairify project created")
    message("Don't forget to edit the file `inst/config.yml` for configuring the data location for your project")
    if (open) {
      usethis::proj_activate(path)
    }
  }
