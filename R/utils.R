#' Wrapper for `system.file` for this package
#'
#' @inherit base::system.file
#'
#' @export
#'
#' @examples
#' pkg_sys()
#'
pkg_sys <- function(
    ...,
    package = utils::packageName(),
    lib.loc = NULL,
    mustWork = FALSE
) {
  system.file(
    ...,
    package = package,
    lib.loc = lib.loc,
    mustWork = mustWork
  )
}


#' Purge a string from all accents and special characters
#'
#' @param x the [character] string to purge
#' @param replacement the [character] to use for replacing special characters
#'
#' @return A purged [character] string
#' @export
#'
#' @examples
#' \dontrun{
#' # Download some funny UTF-8 string
#' tmp <- tempfile()
#' download.file(
#'   file.path("https://raw.githubusercontent.com",
#'             "bits/UTF-8-Unicode-Test-Documents",
#'             "master/UTF-8_sequence_unseparated",
#'             "utf8_sequence_0-0xff_assigned_printable_unseparated.txt", tmp)
#' )
#' txt_utf8 <- readLines(tmp)
#' txt_utf8
#' purge_string(txt_utf8)
#' }
#'
purge_string <- function(x, replacement = " ") {
  iconv(gsub(paste0(replacement, "+"), replacement,
             gsub("&|=|-|:|\\.|'|\\/|<|>|e0|e9|e8|e7|e6|_|\\(|\\)|\"|\\*|#|\u20AC|\\$|\\%",
                  replacement,
                  enc2utf8(x))),
        from = "UTF-8",
        to = "ASCII//TRANSLIT")
}

tinytex_install_babel_language_support <- function(lang) {
    if ("tinytex" %in% rownames(installed.packages())) {
    # Install babel LaTeX package for the specified language
    babel_pkg <- paste0("babel-", lang)
    if (! babel_pkg %in% tinytex::tl_pkgs(only_installed = TRUE)) {
      tinytex::tlmgr_install(babel_pkg)
    }
  }
}
