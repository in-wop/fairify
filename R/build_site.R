#' Build a fairify website
#'
#' @details
#' In order to create a fairiy website containing fairify reports, this function
#' runs successively:
#'
#' - [devtools::build_readme] if a file README.Rmd exists
#' - [pkgdown::build_site]
#' - [render_reports]
#'
#' @inheritParams pkgdown::build_site
#' @inheritParams render_reports
#' @param navbar_reports Add the report list in the navbar of the pkgdown
#' website
#' @param ... Further parameters to pass to [render_reports]
#'
#' @return Used for side effects
#' @export
#'
build_site <- function(pkg = pkgload::pkg_path(),
                       examples = TRUE,
                       run_dont_run = FALSE,
                       seed = 1014,
                       lazy = FALSE,
                       override = list(),
                       preview = NA,
                       devel = TRUE,
                       new_process = !devel,
                       install = !devel,
                       reports_dir = file.path(pkg, "reports"),
                       navbar_reports = TRUE,
                       output_format = c("bookdown::gitbook", "bookdown::pdf_book"),
                       ...) {
  if (file.exists(file.path(pkg, "README.Rmd"))) {
    devtools::build_readme(path = pkg)
  }
  unlink(file.path(pkg, "public"), recursive = TRUE)
  if (navbar_reports) {
    add_reports_pkgdown(pkg, reports_dir)
  }
  pkgdown::build_site(pkg = pkg,
                      examples  = examples,
                      run_dont_run  = run_dont_run,
                      seed  = seed,
                      lazy  = lazy,
                      override  = override,
                      preview  = preview,
                      devel  = devel,
                      new_process  = new_process,
                      install  = install)
  lapply(output_format, function(x) {
    render_reports(reports_dir = reports_dir, output_format = x, ...)
  })
  invisible(TRUE)
}

add_reports_pkgdown <- function(pkg, reports_dir) {
  dfr <- list_reports(reports_dir)
  if (!length(dfr)) return(FALSE)
  f <- file.path(pkg, "_pkgdown.yml")
  if (!file.exists(f)) {
    warning("File not found: ", f, "\nReport list won't be visible in the website navigation bar")
    invisible()
  }
  cpd <- yaml::read_yaml(f)
  cpd$navbar <- add_ifnull(cpd, "navbar")
  cpd$navbar$structure <- add_ifnull(cpd$navbar, "structure")
  cpd$navbar$structure$left <-
    add_ifnull(
      cpd$navbar$structure,
      "left",
      c("intro", "reference", "articles", "tutorials", "news")
    )
  if (!"reports" %in% cpd$navbar$structure$left) {
    cpd$navbar$structure$left <- c("reports", cpd$navbar$structure$left)
  }
  cpd$navbar$components <- add_ifnull(cpd$navbar, "components")
  cpd$navbar$components$reports <- list(text = "Reports")
  pkg <- gsub("/$", "", pkg)
  lr <- apply(dfr, 1, function(r) {
    list(text = r["title"],
         href = file.path(sub(paste0(pkg, "/"), "", r["path"], fixed = TRUE), "index.html"))
  })
  names(lr) <- NULL
  cpd$navbar$components$reports$menu <- lr
  yaml::write_yaml(cpd, f)
}

add_ifnull <- function(l, i, v = list()) {
  if(is.null(l[[i]])) l[[i]] <- v
  return(l[[i]])
}
