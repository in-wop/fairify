#' Source the "setup.R" file of a template
#'
#' @param template Template name in the format `[package]:[name]`
#'
#' @return Nothing, used for side effect
#' @export
#'
#' @examples
#' # Launch basic configuration for reports
#' source_template_setup("fairify:basic")
#'
source_template_setup <- function(template) {
  template_location <- get_template_location(template)
  source(file.path(template_location["path"], "setup.R"))
}
