---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r opts, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# fairify: an R package for creating FAIR reports

<img src="man/figures/logo.png" align="right" style="float: right; height: 220px; margin: 15px;"/>

FAIR principles stands for **F**indability, **A**ccessibility, **I**nteroperability, and **R**euse.

This package provide tools to help you to create HTML and PDF reports based on
[bookdown](https://pkgs.rstudio.com/bookdown/) with data stored on the cloud.

**fairify** currently supports data access Owncloud/Nextcloud servers and provides
templates for Inrae website and PDF reports.

## Installation

```r
# install.packages("remotes")
remotes::install_git(
  "https://forgemia.inra.fr/umr-g-eau/fairify.git",
  dependencies = TRUE
)
```

```{r import_documentationchild, results='asis', echo=FALSE}
s <- readLines("man-examples/fairify.md")
cat(paste(s, collapse = "\n"))
```
