helper_create_fairify <- function(pattern = paste(sample(LETTERS, 8, TRUE), collapse = ""),
                                  path = tempfile(pattern = pattern),
                                  ...) {
  path <- tempfile(pattern = pattern)
  fairify::create_fairify(path, open = FALSE, ...)
  return(path)
}
