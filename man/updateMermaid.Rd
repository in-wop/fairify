% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/update_mermaid.R
\name{updateMermaid}
\alias{updateMermaid}
\title{Update mermaid.js in the DiagrammeR package}
\usage{
updateMermaid(
  url = "https://cdn.jsdelivr.net/npm/mermaid@version/dist/mermaid.min.js",
  version = ""
)
}
\arguments{
\item{url}{\link{character} generic url where to download the file "mermaid.min.js"}

\item{version}{\link{character} specific version to download (eg.: "9.3.0"), use
an empty string for the latest (default)}
}
\value{
Value returned by \link{download.file}
}
\description{
The mermaid js script integrated in DiagrammeR is quiet old. This function
updates the script directly in the DiagrammeR package.

This update can have some issues for particular usages
(See https://github.com/rich-iannone/DiagrammeR/issues/475).
}
\examples{
\dontrun{
updateMermaid()
}
}
